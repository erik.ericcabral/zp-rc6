***Settings***
Documentation       Classe responsável por obter as especificação em BDD

Resource            ../../resources/base.robot


#Executa uma ou masi Keywords antes da execução de todos os steps de cada caso de teste
Test Setup          Start Session
#Executa uma ou mais Keywords apos a execução de todos os steps de cada cado de teste
Suite Teardown      Finish Session
Test Teardown       Finish TestCase

***Test Cases***
Login do Administrador
    Entro na página de Login
    Submeto meu usuario     admin@zepalheta.com.br  pwd123
    Vejo a área logada
