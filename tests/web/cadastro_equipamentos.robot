***Settings***
Documentation       Cadastro de Equipamentos

Resource            ../../resources/base.robot


#Executa uma ou masi Keywords antes da execução de todos os steps de cada caso de teste
Suite Setup          Login Session 
#Executa uma ou mais Keywords apos a execução de todos os steps de cada cado de teste
Suite Teardown       Finish Session
Test Teardown        Finish TestCase


***Test Cases***

Novo Equipamento
    [Tags]      equipo
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...         Guitarra Strinberg        120
    Quando faço a inclusão deste equipamento
    Então devo ver a mensagem:   Equipo cadastrado com sucesso!

#Nome Obrigatório
Campo Nome Obrigatório
    [Tags]      equipo
    [Template]      Validação de Campos
    ${EMPTY}        500                   Nome do equipo é obrigatório

#Valor Obrigatório
Campo Diária é Obrigatório
    [Tags]      equipo
    [Template]      Validação de Campos
    Guitarra Strinberg       ${EMPTY}    Diária do equipo é obrigatória                 

Equipamento Duplicado
    [Tags]      equipo
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o equipamento:
    ...         Guitarra Strinberg        120
    Mas este equipamento já existe no sistema
    Quando faço a inclusão deste equipamento
    Então devo exibir a notificação:   Ocorreu um error na criação de um equipo, tente novamente mais tarde!


***Keywords***
Validação de Campos
    [Arguments]     ${nome}     ${diaria}      ${saida}

    Dado que acesso o formulário de cadastro de equipamentos 
    E que eu tenho o seguinte equipamento:
    ...             ${nome}     ${diaria}      
    Quando faço a inclusão deste equipamento
    Então devo ver o texto:     ${saida}