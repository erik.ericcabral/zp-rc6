***Settings***
Documentation       Exclusão de Clientes

Resource            ../../resources/base.robot


#Executa uma ou masi Keywords antes da execução de todos os steps de cada caso de teste
Suite Setup          Login Session 
#Executa uma ou mais Keywords apos a execução de todos os steps de cada cado de teste
Suite Teardown       Finish Session
Test Teardown        Finish TestCase

# css: ^ = começa com
# CSS: * = contem
# CSS: $ = termina com

***Test Cases***
Remover Cliente
    Dado que eu tenho um cliente indesejado
    ...     Bob Dylan       33333333333     Rua dos Bugs, 2000      21988888888
    E acesso a lista de clientes 
    Quando eu removo esse cliente 
    Então devo ver a notificação:   Cliente removido com sucesso!
    E o cliente não deve aparece na lista



