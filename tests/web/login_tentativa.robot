***Settings***
Documentation       Login Tentativa

Resource            ../../resources/base.robot

#Executa uma ou masi Keywords somente uma única vez antes de todos os caso de teste
Suite Setup         Start Session
#Executa uma ou mais Keywords somente uma única vez após finalizar todos os cado de teste
Suite Teardown      Finish Session
Test Teardown       Finish TestCase
Test Template       Tentativa de login

***Keywords***
Tentativa de login
    [Arguments]     ${input_email}          ${input_senha}          ${output_mensagem}

    Acesso a página de Login            
    Submeto minhas credenciais              ${input_email}          ${input_senha}
    Devo ver um toaster com a mensagem      ${output_mensagem}


***Test Cases***

Senha Incorreta             admin@zepalheta.com.br  abc123          Ocorreu um erro ao fazer login, cheque as credenciais.
Senha em branco             joao@gmail.com          ${EMPTY}        O campo senha é obrigatório!
Email em branco             ${EMPTY}                abc123          O campo email é obrigatório!
Email e Senha em branco     ${EMPTY}                ${EMPTY}        Os campos email e senha não foram preenchidos!
Login Incorreto             admin&gmail.com         abc123          Ocorreu um erro ao fazer login, cheque as credenciais.
  