***Settings***
Documentation       Cadastro de Clientes

Resource            ../../resources/base.robot


#Executa uma ou masi Keywords antes da execução de todos os steps de cada caso de teste
Suite Setup          Login Session 
#Executa uma ou mais Keywords apos a execução de todos os steps de cada cado de teste
Suite Teardown       Finish Session
Test Teardown        Finish TestCase

# css: ^ = começa com
# CSS: * = contem
# CSS: $ = termina com

***Test Cases***

Novo Cliente
    [Tags]      smoke
    Dado que acesso o formulário de cadastro de clientes 
    E que eu tenho o seguinte cliente:
    ...         Bon Jovi        00000001406     Rua dos Bugs, 1000      11999999999
    Quando faço a inclusão deste cliente
    Então devo ver a notificação:   Cliente cadastrado com sucesso!
    E esse cliente deve ser exibido na lista

Cliente Duplicado
    Dado que acesso o formulário de cadastro de clientes 
    E que eu tenho o seguinte cliente:
    ...         Eric Neder      007.455.821-66     Rua dos Bugs, 1000      11999999999
    Mas este CPF já existe no sistema
    Quando faço a inclusão deste cliente
    Então devo ver a notificação de erro:   Este CPF já existe no sistema!

Campos Obrigatórios
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...         ${EMPTY}        ${EMPTY}         ${EMPTY}                 ${EMPTY} 
    Quando faço a inclusão deste cliente
    Então devo ver mensagens informando que os campos do cadastro do cliente são obrigatórios

Campo Nome Obrigatório
    [Tags]      required
    [Template]      Validação de Campos
    ${EMPTY}        48034903094                  Rua dos Bugs, 1000      11999999999        Nome é obrigatório

Campo CPF é Obrigatório
    [Tags]      required
    [Template]      Validação de Campos
    Bon Jovi        ${EMPTY}                     Rua dos Bugs, 1000      11999999999        CPF é obrigatório

Campo Endereço é Obrigatório
    [Tags]      required
    [Template]      Validação de Campos
    Bon Jovi        48034903094                  ${EMPTY}                11999999999        Endereço é obrigatório

Campo Telefone é Obrigatório
    [Tags]      required
    [Template]      Validação de Campos
    Bon Jovi        48034903094                  Rua dos Bugs, 1000      ${EMPTY}           Telefone é obrigatório

Telefone Incorreto
    [Tags]      required
    [Template]      Validação de Campos
    João da Silva   48034903094                  Rua dos Bugs, 1000      123456789          Telefone inválido


***Keywords***
Validação de Campos
    [Arguments]     ${nome}     ${cpf}      ${endereco}     ${telefone}     ${saida}

    Dado que acesso o formulário de cadastro de clientes 
    E que eu tenho o seguinte cliente:
    ...             ${nome}     ${cpf}      ${endereco}     ${telefone}
    Quando faço a inclusão deste cliente
    Então devo ver o texto:     ${saida}
