*** Settings ***

Resource    ../../../resources/services.robot

*** Test Cases ***
Get Customers List

    ${list}=            Get Json  customers/list.json

    FOR                 ${item}     IN      @{list['data']}
       #Log To Console  ${item}  
       Post Custumer    ${item} 
    END

    ${resp}             Get Customers
    Status Should Be    200                 ${resp}
    #Log To Console      ${resp.text}  Imprime a lista de clientes no log
    ${total}=           Get Length          ${resp.json()}
    Should Be True      ${total} > 0

Get Unique Customers

    ${origin}=          Get Json  customers/unique.json

    Delete Custumer     ${origin['cpf']}
    ${resp}=            Post Custumer                   ${origin}
    ${user_id}=         Convert To String               ${resp.json()['id']}

    ${resp}=            Get Unique customer             ${user_id}

    Status Should Be    200                             ${resp}
    Should Be Equal     ${resp.json()['cpf']}           ${origin['cpf']}


Get Not Found

    ${resp}             Get Unique customer             698dc19d489c4e4db73e28a713eab07b       
    Status Should Be    404                             ${resp}
    Should Be Equal     ${resp.json()['message']}       Customer not found
