*** Settings ***

Resource    ../../../resources/services.robot

*** Test Cases ***

Delete Customer

    ${origin}=          Get Json  customers/delete.json

    Delete Custumer     ${origin['cpf']}
    ${resp}=            Post Custumer                   ${origin}

    ${resp}=            Delete Custumer                 ${origin['cpf']}

    Status Should Be    204                             ${resp}

Get Not Found

    ${resp}             Delete Custumer                 594.915.850-45       
    Status Should Be    404                             ${resp}
    Should Be Equal     ${resp.json()['message']}       Customer not found
