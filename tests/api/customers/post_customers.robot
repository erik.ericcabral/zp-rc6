***Settings***
Documentation       Implementação do crud da apid

Resource    ../../../resources/services.robot

***Test Cases***
New Customers

    #&{payload}=         Create Dictionary       name=Fleia          cpf=777.777.777-77      address=Rua dos bugs, 1000     phone_number=(11) 9.7777-7777
    #${json_file}            Get File                ${EXECDIR}/resources/fixtures/customers/flea.json 
    ${payload}=             Get Json                customers/flea.json

    Delete Custumer     ${payload['cpf']} 

    ${resp}=            Post Custumer           ${payload}

    Status Should Be    200                     ${resp}


Name is required

    ${payload}=         Get Json                customers/no_name.json 
    ${resp}=            Post Custumer           ${payload}

    Status Should Be    400                     ${resp}
    Should Be Equal     ${resp.json()['message']}                   "name" is required


CPF is required

    ${payload}=         Get Json                customers/no_cpf.json 
    ${resp}=            Post Custumer           ${payload}

    Status Should Be    400                     ${resp}
    Should Be Equal     ${resp.json()['message']}                   "cpf" is required


Address is required

    ${payload}=         Get Json                customers/no_address.json 
    ${resp}=            Post Custumer           ${payload}

    Status Should Be    400                     ${resp}
    Should Be Equal     ${resp.json()['message']}                   "address" is required


Phone is required

    ${payload}=         Get Json                customers/no_phone.json 
    ${resp}=            Post Custumer           ${payload}

    Status Should Be    400                     ${resp}
    Should Be Equal     ${resp.json()['message']}                   "phone_number" is required


