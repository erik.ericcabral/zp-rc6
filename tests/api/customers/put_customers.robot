*** Settings ***

Resource    ../../../resources/services.robot

*** Test Cases ***
Update a Customers

    #Cliente origem
    ${payload}=         Get Json  customers/slash.json

    #Deletando o cliente criado anteriormente
    Delete Custumer  ${payload['cpf']}

    #Cadastra o cliente
    ${resp}=             Post Custumer           ${payload}

    #Pega o id do cliente
    ${user_id}=         Convert to String       ${resp.json()['id']}

    #Muda a massa para um novo nome
    Set To Dictionary   ${payload}              name            Saul Hudson

    #Solicita a alteração na API
    ${resp}=            Put Custumer            ${payload}      ${user_id}   
    
    Status Should Be    204                     ${resp}    

    #Obtem os dados do cliente para validar se o nome foi alterado
    ${resp}             Get Unique customer     ${user_id}

    Should Be Equal     ${resp.json()['name']}  Saul Hudson