***Settings***
Documentation       Classe destinada a obter todas as Keywords do Cadastro de Clientes

***Keywords***

#Login do Administrador
Entro na página de Login
    Go To           ${base_url} 

Submeto meu usuario
    [Arguments]     ${email}        ${password}

    Login With      ${email}        ${password}

Vejo a área logada
    Wait Until Page Contains        Aluguéis    10   

#Senha, email Incorreta
Devo ver um toaster com a mensagem 
    [Arguments]     ${expect_message} 

    Wait Until Element Contains     ${TOASTER_ERROR}       ${expect_message}       30

#Customers

Dado que acesso o formulário de cadastro de clientes
    Go To Customers 
    Wait Until Element Is Visible   ${CUSTOMERS_FORM}     5   #aumentado o sllep por causa do headless
    Click Element                   ${CUSTOMERS_FORM}

E que eu tenho o seguinte cliente:
    [Arguments]     ${nome}          ${cpf}          ${address}         ${phone_number}

    Remove_Customer_By_Cpf           ${cpf}

    Set Test Variable       ${nome}
    Set Test Variable       ${cpf}
    Set Test Variable       ${address}
    Set Test Variable       ${phone_number}

Mas este CPF já existe no sistema
    Insert_customer         ${nome}  ${cpf}  ${address}  ${phone_number}

Quando faço a inclusão deste cliente
    Register New Customers   ${nome}  ${cpf}  ${address}  ${phone_number}

Então devo ver a notificação:
    [Arguments]     ${expect_notice}

    Wait Until Element Contains      ${TOASTER_SUCCESS}        ${expect_notice}          500

Então devo ver a notificação de erro:
    [Arguments]     ${expect_notice}

    Wait Until Element Contains      ${TOASTER_ERROR_V2}       ${expect_notice}          10

Então devo ver mensagens informando que os campos do cadastro do cliente são obrigatórios

    Wait Until Element Contains        ${LABEL_NAME}           Nome é obrigatório        5
    Wait Until Element Contains        ${LABEL_CPF}            CPF é obrigatório         5
    Wait Until Element Contains        ${LABEL_ADDRESS}        Endereço é obrigatório    5
    Wait Until Element Contains        ${LABEL_PHONE}          Telefone é obrigatório    5

Então devo ver o texto:             
    [Arguments]                        ${expect_text}          

    Wait Until Page Contains           ${expect_text}          500

E esse cliente deve ser exibido na lista
    ${cpf_formatado}=                  format_cpf              ${cpf} 
    Go Back  #utiliza a seta voltar do navegador
    Wait Until Element Is Visible      ${CUSTOMERS_LIST}       500
    Table Should Contain               ${CUSTOMERS_LIST}       ${cpf_formatado}

#Remove o cliente indesejado
Dado que eu tenho um cliente indesejado
    [Arguments]                 ${name}     ${cpf}      ${address}     ${phone_number}

    remove_customer_by_cpf                  ${cpf} 
    Insert Customer             ${name}     ${cpf}      ${address}     ${phone_number}         
    Set Test Variable                       ${cpf}      #Variavel para testes

E acesso a lista de clientes 
    Go To Customers

Quando eu removo esse cliente 

    ${cpf_formatado}=                   format_cpf      ${cpf}
    Set Test Variable                   ${cpf_formatado}

    Go To Customres datails             ${cpf_formatado}
    Click Remove Customres

E o cliente não deve aparece na lista
    Wait Until Page Does Not Contain    ${cpf_formatado}

########## Contrato de Locação ####################

Dado que eu tenho o seguinte cliente cadastrado:
    [Arguments]     ${file_name}

    ${customer}=     Get Json  customers/${file_name}

    Delete Custumer     ${customer['cpf']} 
    Post Custumer       ${customer}
    Set Test Variable   ${customer}

E este cliente deseja alugar o seguinte equipo:      
    [Arguments]     ${file_name}

    ${equipo}=     Get Json  equipos/${file_name}

    Post Equipo     ${equipo}
    Set Test Variable   ${equipo}

E acesso o formulário de contratos
    Go To Contracts
    Click Element       ${CONTRACTS_FORM}

Quando faço um novo contrato de locação
    Create a new Contract  ${customer['name']}  ${equipo['name']}




