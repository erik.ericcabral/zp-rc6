import psycopg2

def execute_q(query):

    conn = psycopg2.connect(
        host = 'zepalheta-postgres',  
        database = 'zepalheta',
        user = 'postgres',
        password = 'qaninja'
    )

    cur = conn.cursor()
    cur.execute(query)
    conn.commit()

    cur.close()
    conn.close()

def format_cpf(cpf):
    return cpf[:3] + "." + cpf[3:6] + "." + cpf[6:9] + "-" + cpf[9:]


def Insert_customer(nome, cpf, address, phone_number):

    cpf_formatado = format_cpf(cpf)

    query = "insert into public.customers(name, cpf, address, phone_number) "\
    "values ('{}', '{}', '{}', '{}');".format(nome, cpf_formatado, address, phone_number)

    execute_q(query)


def remove_customer_by_cpf(cpf):

    cpf_formatado = format_cpf(cpf)

    query = "delete from public.customers where cpf = '{}';".format(cpf_formatado)
    execute_q(query)

def Insert_equipo(nome, diaria):

    query = "insert into public.equipos(name, daily_price) "\
    "values ('{}', '{}');".format(nome, diaria)

    execute_q(query)

def remove_equipo_by_name(nome):

    query = "delete from public.equipos where name = '{}';".format(nome)
    #print(query)
    execute_q(query)