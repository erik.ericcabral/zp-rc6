***Settings***
Documentation       Representação do menu lateral de navegação na área logada

***Variables***
${NAV_CUSTOMERS}        css:a[href$=customers]
${NAV_CONTRACTS}        css:a[href$=contracts]
${NAV_EQUIPO}           css:a[href$=equipos]
                            

***Keywords***
Go To Customers
    Wait Until Element Is Visible   ${NAV_CUSTOMERS}      500   #aumentado o sllep por causa do headless
    Click Element                   ${NAV_CUSTOMERS} 

Go To Contracts
    Wait Until Element Is Visible   ${NAV_CONTRACTS}      500   #aumentado o sllep por causa do headless
    Click Element                   ${NAV_CONTRACTS}