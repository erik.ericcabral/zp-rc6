***Settings***
Documentation       Representação da página Equipamentos com seus elementos

***Variables***
${EQUIPOS_FORM}         css:a[href$=register]   #botao cadastrar
${LABEL_NAME}           css:label[for=name]
${LABEL_DIARIA}         css:label[for=price]




***Keywords***
Register New Equipos
    [Arguments]     ${nome}                  ${diaria}

    Input Text      id:equipo-name           ${nome}
    Input Text      id:daily_price           ${diaria}

    Click Element   xpath://button[text()='CADASTRAR']