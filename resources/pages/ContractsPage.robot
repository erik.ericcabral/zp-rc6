***Settings***
Documentation       Representação da página contratos de aluguéis

***Variables***
${CONTRACTS_FORM}       css:a[href$=register]


***Keywords***
Create a new Contract
    [Arguments]     ${customer_name}         ${equipo_name}

    Click Element       xpath://div[contains(text(), 'Escolha o locatário')]
    Click Element       xpath://div[contains(text(), '${customer_name}')]
    #sleep               10
    #${code}             Get Source      #pega o codigo fonte da página HTML
    #Log                 ${code}         #Atribui o codigo fonte no log
    Click Element       xpath://div[contains(text(), 'Escolha o item')]
    Click Element       xpath://div[contains(text(), '${equipo_name}')]

    Input text          id:delivery_price       10
    Input text          id:quantity             1

    Click Element       css:button[type=submit]
    click Element       xpath://button[text()='CADASTRAR']
    sleep               10
