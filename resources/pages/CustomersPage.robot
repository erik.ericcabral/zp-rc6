***Settings***
Documentation       Representação da página clientes com seus elementos

***Variables***
${CUSTOMERS_FORM}       css:a[href$=register]
${LABEL_NAME}           css:label[for=name]
${LABEL_CPF}            css:label[for=cpf]
${LABEL_ADDRESS}        css:label[for=address]
${LABEL_PHONE}          css:label[for=phone_number]
${CUSTOMERS_LIST}       css:table



***Keywords***
Register New Customers 
    [Arguments]     ${nome}          ${cpf}          ${address}         ${phone_number}

    Reload Page

    Input Text      id:name          ${nome}
    Input Text      id:cpf           ${cpf}
    Input Text      id:address       ${address}
    Input Text      id:phone_number  ${phone_number}

    Click Element   xpath://button[text()='CADASTRAR']

Go To Customres datails
    [Arguments]     ${cpf_formatado}

    ${clica_detalhes} =                 Set Variable            xpath://td[text()="${cpf_formatado}"] 
    Wait Until Element Is Visible       ${clica_detalhes}       500
    Click Element                       ${clica_detalhes}

Click Remove Customres
    ${clica_remover} =                  Set Variable            xpath://button[text()="APAGAR"] 

    Wait Until Element Is Visible       ${clica_remover}        5
    Click Element                       ${clica_remover}


