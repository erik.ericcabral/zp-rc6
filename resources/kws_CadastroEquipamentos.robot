***Settings***
Documentation       Classe destinada a obter todas as Keywords do Cadastro de Equipamentos

***Keywords***

#Login do Administrador
Acesso a página de Login
    Go To           ${base_url} 

Submeto minhas credenciais
    [Arguments]     ${email}        ${password}

    Login With      ${email}        ${password}

Devo ver a área logada
    Wait Until Page Contains        Equipos         10


Dado que acesso o formulário de cadastro de equipamentos
    Wait Until Element Is Visible   ${NAV_EQUIPO}         500
    Click Element                   ${NAV_EQUIPO}         
    Wait Until Element Is Visible   ${EQUIPOS_FORM}       500
    Click Element                   ${EQUIPOS_FORM} 

E que eu tenho o seguinte equipamento:
    [Arguments]     ${nome}         ${diaria}         

    remove_equipo_by_name           ${nome}

    Set Test Variable               ${nome}
    Set Test Variable               ${diaria}

E que eu tenho o equipamento:
    [Arguments]     ${nome}         ${diaria}         

    Set Test Variable               ${nome}
    Set Test Variable               ${diaria}

Mas este equipamento já existe no sistema
    Insert_equipo                   ${nome}  ${diaria}  
   
Quando faço a inclusão deste equipamento
    Register New Equipos            ${nome}  ${diaria}  

Então devo ver a mensagem:  
    [Arguments]     ${expect_notice}

    Wait Until Element Contains     ${TOASTER_SUCCESS}        ${expect_notice}          500

Então devo exibir a notificação:  
    [Arguments]     ${expect_notice}

    Wait Until Element Contains     ${TOASTER_ERROR}          ${expect_notice}          10