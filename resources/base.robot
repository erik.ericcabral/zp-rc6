***Settings***
Documentation       Classe responsável por obter os ganchos das classes de testes

Library     SeleniumLibrary

Library     libs/db.py

Resource    kws_CadastroClientes.robot
Resource    kws_CadastroEquipamentos.robot
Resource    hooks.robot
Resource    services.robot

Resource    pages/CustomersPage.robot
Resource    pages/EquiposPage.robot
Resource    components/Toaster.robot
Resource    components/Sidebar.robot
Resource    pages/LoginPage.robot
Resource    pages/ContractsPage.robot


***Variables***

${base_url}         http://zepalheta-web:3000/

${admin_user}       admin@zepalheta.com.br  
${admin_pass}       pwd123
