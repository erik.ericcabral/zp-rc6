***Settings***
Documentation       Classe responsavel por conter toda a implementação inicial e final de cada caso de testes

***Keywords***

Start Session

    Run Keyword If      "${browser}" == "headless"
    ...     Open Chrome Headless

    Run Keyword If      "${browser}" == "chrome"
    ...     Open chrome

    Set Window Size     1400    900

Finish TestCase
    Capture Page Screenshot


Finish Session
    Close Browser

Login Session

    Start Session
    Go To            ${base_url} 
    Login With       ${admin_user}      ${admin_pass} 

### Webdriver
Open chrome Headless
    Open Browser        about:blank     headlesschrome      options=add_argument('--disable-dev-shm-usage')

Open chrome
    Open Browser        about:blank     chrome              options=add_experimental_option('excludeSwitches', ['enable-logging'])